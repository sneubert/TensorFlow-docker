
FROM apiucci/ubuntu16-04:latest

MAINTAINER Alessio Piucci <alessio.piucci@cern.ch>

USER root

RUN apt-get -y update

##########
## install TensorFlow
##########

#tensorflow related stuff

# install pip for Python 2.7
RUN apt-get -y install python-pip python-dev

# setup the work directory
WORKDIR /tmp

########

# install tensorflow, with GPU support

# install the NVidia drivers
RUN apt-get -y install software-properties-common \
    && apt-get -y update \
    && apt-get purge nvidia* \
    && add-apt-repository ppa:graphics-drivers/ppa \
    && apt-get -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install nvidia-384 nvidia-settings

# install CUDA 9.0
# I need to get the .deb package from somewhere: until know, I have it in a GitLab repo
RUN cd /tmp; git clone --recursive https://gitlab.cern.ch/apiucci/TensorFlow-docker.git \
    && cd TensorFlow-docker ; dpkg -i cuda-repo-ubuntu1604_9.0.176-1_amd64.deb

# add the repo keys
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub

# finally install CUDA, disabling the interactive input required to set the keyboard configuration
# for info: https://stackoverflow.com/questions/38165407/installing-lightdm-in-dockerfile-raises-interactive-keyboard-layout-menu
RUN apt-get -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install cuda

# set some env variables
RUN cd ; echo 'export CUDA_HOME=/usr/local/cuda-9.0' >> .bashrc \
    && echo 'export PATH=$CUDA_HOME/bin:$PATH:$HOME/bin' >> .bashrc \
    && echo 'export LD_LIBRARY_PATH=$CUDA_HOME/lib64:$LD_LIBRARY_PATH' >> .bashrc

# install cuDNN 7.0 for CUDA 9.0
# read here for instructions: https://askubuntu.com/questions/767269/how-can-i-install-cudnn-on-ubuntu-16-04
# I need to get the cuDNN package from somewhere: until know, I have it in a GitLab repo
RUN cd /tmp/TensorFlow-docker; tar -xzvf cudnn-9.0-linux-x64-v7.tgz \
    && cd cuda ; cp include/cudnn.h /usr/local/cuda-9.0/include/ ; cp lib64/libcudnn* /usr/local/cuda-9.0/lib64/ \
    && chmod a+r /usr/local/cuda-9.0/lib64/libcudnn*

# install the CUDA Profile Tools interface
RUN apt-get -y install libcupti-dev

#install tensorflow, with GPU support
RUN pip install tensorflow-gpu

# currently, CUDA 9.0 and cuDNN 7.0 are too updated wrt Tensorflow 1.3
# we have to wait the new Tensorflow 1.4 release
# in the mean while, and ugly hack is to create symbolic links...
# for info: https://stackoverflow.com/questions/46595292/solved-virtualenv-tensorflow-with-nvidia-gpu-cuda-9-0-vs-cuda-8-0-cudnn-7-0
RUN ln -s /usr/local/cuda-9.0/lib64/libcusolver.so.9.0 /usr/local/cuda-9.0/lib64/libcusolver.so.8.0 \
    && ln -s /usr/local/cuda-9.0/lib64/libcufft.so.9.0 /usr/local/cuda-9.0/lib64/libcufft.so.8.0 \
    && ln -s /usr/local/cuda-9.0/lib64/libcublas.so.9.0 /usr/local/cuda-9.0/lib64/libcublas.so.8.0 \
    && ln -s /usr/local/cuda-9.0/lib64/libcurand.so.9.0 /usr/local/cuda-9.0/lib64/libcurand.so.8.0 \
    && ln -s /usr/local/cuda-9.0/lib64/libcudart.so.9.0 /usr/local/cuda-9.0/lib64/libcudart.so.8.0 \
    && ln -s /usr/local/cuda-9.0/lib64/libcudnn.so.7 /usr/local/cuda-9.0/lib64/libcudnn.so.6