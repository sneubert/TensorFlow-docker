
#script to test the TensorFlow installation

import tensorflow as tf

message = tf.constant("\nTensorFlow works, now trying to import ROOT.")
sess = tf.Session()

print(sess.run(message))

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT

message = tf.constant("The ROOT libraries are imported.\n-->Everything is correctly set, that's amazing!\n")
sess = tf.Session()

print(sess.run(message))
